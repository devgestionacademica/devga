USE [master]
GO
/****** Object:  Database [dbga]    Script Date: 5/05/2019 9:26:39 p. m. ******/
CREATE DATABASE [dbga]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'dbga', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\dbga.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'dbga_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\dbga_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [dbga] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dbga].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dbga] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dbga] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dbga] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dbga] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dbga] SET ARITHABORT OFF 
GO
ALTER DATABASE [dbga] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [dbga] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dbga] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dbga] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dbga] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [dbga] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dbga] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dbga] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dbga] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dbga] SET  DISABLE_BROKER 
GO
ALTER DATABASE [dbga] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dbga] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [dbga] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [dbga] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [dbga] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dbga] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [dbga] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [dbga] SET RECOVERY FULL 
GO
ALTER DATABASE [dbga] SET  MULTI_USER 
GO
ALTER DATABASE [dbga] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [dbga] SET DB_CHAINING OFF 
GO
ALTER DATABASE [dbga] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [dbga] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [dbga] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'dbga', N'ON'
GO
ALTER DATABASE [dbga] SET QUERY_STORE = OFF
GO
USE [dbga]
GO
/****** Object:  Table [dbo].[tblAcceso]    Script Date: 5/05/2019 9:26:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAcceso](
	[idAcceso] [int] IDENTITY(1,1) NOT NULL,
	[permiso] [varchar](50) NOT NULL,
	[idRol] [int] NOT NULL,
 CONSTRAINT [PK_tblAcceso] PRIMARY KEY CLUSTERED 
(
	[idAcceso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblAsignatura]    Script Date: 5/05/2019 9:26:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAsignatura](
	[idAsignatura] [int] IDENTITY(1,1) NOT NULL,
	[nombreAsignatura] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblAsignatura] PRIMARY KEY CLUSTERED 
(
	[idAsignatura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblAsignaXDiplomado]    Script Date: 5/05/2019 9:26:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAsignaXDiplomado](
	[idAsigDiplo] [int] IDENTITY(1,1) NOT NULL,
	[idAsignatura] [int] NOT NULL,
	[idDiplomado] [int] NOT NULL,
	[caracter] [varchar](50) NOT NULL,
	[idProfesor] [int] NOT NULL,
	[creditos] [nchar](10) NOT NULL,
 CONSTRAINT [PK_tblAsignaXDiplomado] PRIMARY KEY CLUSTERED 
(
	[idAsigDiplo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblDiplomado]    Script Date: 5/05/2019 9:26:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblDiplomado](
	[idDiplomado] [int] IDENTITY(1,1) NOT NULL,
	[nombreDiplomado] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblDiplomado] PRIMARY KEY CLUSTERED 
(
	[idDiplomado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblEstXDiploXAsigna]    Script Date: 5/05/2019 9:26:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblEstXDiploXAsigna](
	[idEstxDiploxAsigna] [int] IDENTITY(1,1) NOT NULL,
	[idAsigXDiplo] [int] NOT NULL,
	[idUsuario] [int] NOT NULL,
	[periodo] [varchar](50) NOT NULL,
	[notaF] [float] NOT NULL,
 CONSTRAINT [PK_tblEstXDiploXAsigna] PRIMARY KEY CLUSTERED 
(
	[idEstxDiploxAsigna] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMatricula]    Script Date: 5/05/2019 9:26:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblMatricula](
	[idMatricula] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[idDiplomado] [int] NOT NULL,
	[fecha] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblMatricula] PRIMARY KEY CLUSTERED 
(
	[idMatricula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblNota]    Script Date: 5/05/2019 9:26:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblNota](
	[idNota] [int] IDENTITY(1,1) NOT NULL,
	[idEstXDiploXAsign] [int] NOT NULL,
	[nota1] [float] NULL,
	[nota2] [float] NULL,
	[nota3] [float] NULL,
 CONSTRAINT [PK_tblNota] PRIMARY KEY CLUSTERED 
(
	[idNota] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblPago]    Script Date: 5/05/2019 9:26:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblPago](
	[idPago] [int] IDENTITY(1,1) NOT NULL,
	[idMatricula] [int] NOT NULL,
	[fecha] [varchar](50) NOT NULL,
	[valorTotal] [bigint] NOT NULL,
 CONSTRAINT [PK_tblPago] PRIMARY KEY CLUSTERED 
(
	[idPago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblProfesorXAsignatura]    Script Date: 5/05/2019 9:26:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblProfesorXAsignatura](
	[idUsuario] [int] NOT NULL,
	[idAsignatura] [int] NOT NULL,
	[idPRofexAsig] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_tblProfesorXAsignatura] PRIMARY KEY CLUSTERED 
(
	[idPRofexAsig] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblRol]    Script Date: 5/05/2019 9:26:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRol](
	[idRol] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tblRol] PRIMARY KEY CLUSTERED 
(
	[idRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUsuario]    Script Date: 5/05/2019 9:26:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUsuario](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[contrasena] [varchar](50) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido] [varchar](50) NOT NULL,
	[correo] [varchar](50) NULL,
	[telefono] [int] NULL,
	[direccion] [varchar](50) NULL,
	[idRol] [int] NOT NULL,
 CONSTRAINT [PK_tblUsuario] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblAcceso] ON 
GO
INSERT [dbo].[tblAcceso] ([idAcceso], [permiso], [idRol]) VALUES (1, N'editar profesores', 1)
GO
INSERT [dbo].[tblAcceso] ([idAcceso], [permiso], [idRol]) VALUES (2, N'actualizar diplomados', 2)
GO
SET IDENTITY_INSERT [dbo].[tblAcceso] OFF
GO
SET IDENTITY_INSERT [dbo].[tblDiplomado] ON 
GO
INSERT [dbo].[tblDiplomado] ([idDiplomado], [nombreDiplomado]) VALUES (1, N'ing s')
GO
INSERT [dbo].[tblDiplomado] ([idDiplomado], [nombreDiplomado]) VALUES (2, N'ing teleco')
GO
INSERT [dbo].[tblDiplomado] ([idDiplomado], [nombreDiplomado]) VALUES (3, N'ing aeronautica')
GO
SET IDENTITY_INSERT [dbo].[tblDiplomado] OFF
GO
SET IDENTITY_INSERT [dbo].[tblMatricula] ON 
GO
INSERT [dbo].[tblMatricula] ([idMatricula], [idUsuario], [idDiplomado], [fecha]) VALUES (1, 1, 1, N'11/12/1998')
GO
SET IDENTITY_INSERT [dbo].[tblMatricula] OFF
GO
SET IDENTITY_INSERT [dbo].[tblPago] ON 
GO
INSERT [dbo].[tblPago] ([idPago], [idMatricula], [fecha], [valorTotal]) VALUES (1, 1, N'1998', 200000)
GO
SET IDENTITY_INSERT [dbo].[tblPago] OFF
GO
SET IDENTITY_INSERT [dbo].[tblRol] ON 
GO
INSERT [dbo].[tblRol] ([idRol], [nombre]) VALUES (1, N'profesor')
GO
INSERT [dbo].[tblRol] ([idRol], [nombre]) VALUES (2, N'administrador')
GO
INSERT [dbo].[tblRol] ([idRol], [nombre]) VALUES (3, N'alumno')
GO
SET IDENTITY_INSERT [dbo].[tblRol] OFF
GO
SET IDENTITY_INSERT [dbo].[tblUsuario] ON 
GO
INSERT [dbo].[tblUsuario] ([idUsuario], [contrasena], [nombre], [apellido], [correo], [telefono], [direccion], [idRol]) VALUES (1, N'123', N'jarol', N'medina', N'jarolmedina', 234234, N'carrera90c', 1)
GO
INSERT [dbo].[tblUsuario] ([idUsuario], [contrasena], [nombre], [apellido], [correo], [telefono], [direccion], [idRol]) VALUES (2, N'123', N'diana', N'rodirguez', N'diana', 234, N'carreara', 2)
GO
INSERT [dbo].[tblUsuario] ([idUsuario], [contrasena], [nombre], [apellido], [correo], [telefono], [direccion], [idRol]) VALUES (3, N'1234', N'Carola', N'Rodriguez', N'dicriroga18200@gmail.com', 486512, N'Cll 79a #69 - 33', 3)
GO
SET IDENTITY_INSERT [dbo].[tblUsuario] OFF
GO
ALTER TABLE [dbo].[tblAcceso]  WITH CHECK ADD  CONSTRAINT [FK_tblAcceso_tblRol] FOREIGN KEY([idRol])
REFERENCES [dbo].[tblRol] ([idRol])
GO
ALTER TABLE [dbo].[tblAcceso] CHECK CONSTRAINT [FK_tblAcceso_tblRol]
GO
ALTER TABLE [dbo].[tblAsignaXDiplomado]  WITH CHECK ADD  CONSTRAINT [FK_tblAsignaXDiplomado_tblAsignatura] FOREIGN KEY([idAsignatura])
REFERENCES [dbo].[tblAsignatura] ([idAsignatura])
GO
ALTER TABLE [dbo].[tblAsignaXDiplomado] CHECK CONSTRAINT [FK_tblAsignaXDiplomado_tblAsignatura]
GO
ALTER TABLE [dbo].[tblAsignaXDiplomado]  WITH CHECK ADD  CONSTRAINT [FK_tblAsignaXDiplomado_tblDiplomado] FOREIGN KEY([idDiplomado])
REFERENCES [dbo].[tblDiplomado] ([idDiplomado])
GO
ALTER TABLE [dbo].[tblAsignaXDiplomado] CHECK CONSTRAINT [FK_tblAsignaXDiplomado_tblDiplomado]
GO
ALTER TABLE [dbo].[tblEstXDiploXAsigna]  WITH CHECK ADD  CONSTRAINT [FK_tblEstXDiploXAsigna_tblAsignaXDiplomado] FOREIGN KEY([idAsigXDiplo])
REFERENCES [dbo].[tblAsignaXDiplomado] ([idAsigDiplo])
GO
ALTER TABLE [dbo].[tblEstXDiploXAsigna] CHECK CONSTRAINT [FK_tblEstXDiploXAsigna_tblAsignaXDiplomado]
GO
ALTER TABLE [dbo].[tblEstXDiploXAsigna]  WITH CHECK ADD  CONSTRAINT [FK_tblEstXDiploXAsigna_tblUsuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[tblUsuario] ([idUsuario])
GO
ALTER TABLE [dbo].[tblEstXDiploXAsigna] CHECK CONSTRAINT [FK_tblEstXDiploXAsigna_tblUsuario]
GO
ALTER TABLE [dbo].[tblMatricula]  WITH CHECK ADD  CONSTRAINT [FK_tblMatricula_tblDiplomado] FOREIGN KEY([idDiplomado])
REFERENCES [dbo].[tblDiplomado] ([idDiplomado])
GO
ALTER TABLE [dbo].[tblMatricula] CHECK CONSTRAINT [FK_tblMatricula_tblDiplomado]
GO
ALTER TABLE [dbo].[tblMatricula]  WITH CHECK ADD  CONSTRAINT [FK_tblMatricula_tblUsuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[tblUsuario] ([idUsuario])
GO
ALTER TABLE [dbo].[tblMatricula] CHECK CONSTRAINT [FK_tblMatricula_tblUsuario]
GO
ALTER TABLE [dbo].[tblNota]  WITH CHECK ADD  CONSTRAINT [FK_tblNota_tblEstXDiploXAsigna] FOREIGN KEY([idEstXDiploXAsign])
REFERENCES [dbo].[tblEstXDiploXAsigna] ([idEstxDiploxAsigna])
GO
ALTER TABLE [dbo].[tblNota] CHECK CONSTRAINT [FK_tblNota_tblEstXDiploXAsigna]
GO
ALTER TABLE [dbo].[tblPago]  WITH CHECK ADD  CONSTRAINT [FK_tblPago_tblMatricula] FOREIGN KEY([idMatricula])
REFERENCES [dbo].[tblMatricula] ([idMatricula])
GO
ALTER TABLE [dbo].[tblPago] CHECK CONSTRAINT [FK_tblPago_tblMatricula]
GO
ALTER TABLE [dbo].[tblProfesorXAsignatura]  WITH CHECK ADD  CONSTRAINT [FK_tblProfesorXAsignatura_tblAsignatura] FOREIGN KEY([idAsignatura])
REFERENCES [dbo].[tblAsignatura] ([idAsignatura])
GO
ALTER TABLE [dbo].[tblProfesorXAsignatura] CHECK CONSTRAINT [FK_tblProfesorXAsignatura_tblAsignatura]
GO
ALTER TABLE [dbo].[tblProfesorXAsignatura]  WITH CHECK ADD  CONSTRAINT [FK_tblProfesorXAsignatura_tblUsuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[tblUsuario] ([idUsuario])
GO
ALTER TABLE [dbo].[tblProfesorXAsignatura] CHECK CONSTRAINT [FK_tblProfesorXAsignatura_tblUsuario]
GO
ALTER TABLE [dbo].[tblUsuario]  WITH CHECK ADD  CONSTRAINT [FK_tblUsuario_tblRol] FOREIGN KEY([idRol])
REFERENCES [dbo].[tblRol] ([idRol])
GO
ALTER TABLE [dbo].[tblUsuario] CHECK CONSTRAINT [FK_tblUsuario_tblRol]
GO
USE [master]
GO
ALTER DATABASE [dbga] SET  READ_WRITE 
GO
