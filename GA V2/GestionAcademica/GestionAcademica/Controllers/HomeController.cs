﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace GestionAcademica.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string message = "")
        {
            ViewBag.Message = message;
            return View();
        }

        [HttpPost]
        public ActionResult Login(string email, string password)
        {
            if(!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password))
            {
                BdPruebaEntities db = new BdPruebaEntities();
                var user = db.tblUsuarios.FirstOrDefault(e => e.correo == email && e.contrasena == password);
                
                if(user != null && user.idRol == 2)
                {
                    FormsAuthentication.SetAuthCookie(user.correo, true);
                    return RedirectToAction("Index", "Profile");
                }
                else if(user != null && user.idRol == 1)
                {
                    FormsAuthentication.SetAuthCookie(user.correo, true);
                    return RedirectToAction("Profesor", "Profile");
                }

                else
                {
                    return RedirectToAction("Index", new { message = "Usuario y/o contraseña incorrectos" });
                }
            }
            else
            {
                return RedirectToAction("Index", new { message = "Ingresa el usuario y contraseña" });
            }
        }

        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }
        
    }
}