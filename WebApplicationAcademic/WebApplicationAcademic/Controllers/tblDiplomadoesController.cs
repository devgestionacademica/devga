﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplicationAcademic;

namespace WebApplicationAcademic.Controllers
{
    public class tblDiplomadoesController : Controller
    {
        private dbgaEntities db = new dbgaEntities();

        // GET: tblDiplomadoes
        public ActionResult Index()
        {
            return View(db.tblDiplomadoes.ToList());
        }

        // GET: tblDiplomadoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblDiplomado tblDiplomado = db.tblDiplomadoes.Find(id);
            if (tblDiplomado == null)
            {
                return HttpNotFound();
            }
            return View(tblDiplomado);
        }

        // GET: tblDiplomadoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tblDiplomadoes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idDiplomado,nombreDiplomado")] tblDiplomado tblDiplomado)
        {
            if (ModelState.IsValid)
            {
                db.tblDiplomadoes.Add(tblDiplomado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblDiplomado);
        }

        // GET: tblDiplomadoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblDiplomado tblDiplomado = db.tblDiplomadoes.Find(id);
            if (tblDiplomado == null)
            {
                return HttpNotFound();
            }
            return View(tblDiplomado);
        }

        // POST: tblDiplomadoes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idDiplomado,nombreDiplomado")] tblDiplomado tblDiplomado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblDiplomado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblDiplomado);
        }

        // GET: tblDiplomadoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblDiplomado tblDiplomado = db.tblDiplomadoes.Find(id);
            if (tblDiplomado == null)
            {
                return HttpNotFound();
            }
            return View(tblDiplomado);
        }

        // POST: tblDiplomadoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblDiplomado tblDiplomado = db.tblDiplomadoes.Find(id);
            db.tblDiplomadoes.Remove(tblDiplomado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
