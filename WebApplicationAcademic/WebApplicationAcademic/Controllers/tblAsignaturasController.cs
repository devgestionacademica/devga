﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplicationAcademic;

namespace WebApplicationAcademic.Controllers
{
    public class tblAsignaturasController : Controller
    {
        private dbgaEntities db = new dbgaEntities();

        // GET: tblAsignaturas
        public ActionResult Index()
        {
            return View(db.tblAsignaturas.ToList());
        }

        // GET: tblAsignaturas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblAsignatura tblAsignatura = db.tblAsignaturas.Find(id);
            if (tblAsignatura == null)
            {
                return HttpNotFound();
            }
            return View(tblAsignatura);
        }

        // GET: tblAsignaturas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: tblAsignaturas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idAsignatura,nombreAsignatura")] tblAsignatura tblAsignatura)
        {
            if (ModelState.IsValid)
            {
                db.tblAsignaturas.Add(tblAsignatura);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblAsignatura);
        }

        // GET: tblAsignaturas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblAsignatura tblAsignatura = db.tblAsignaturas.Find(id);
            if (tblAsignatura == null)
            {
                return HttpNotFound();
            }
            return View(tblAsignatura);
        }

        // POST: tblAsignaturas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idAsignatura,nombreAsignatura")] tblAsignatura tblAsignatura)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblAsignatura).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblAsignatura);
        }

        // GET: tblAsignaturas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblAsignatura tblAsignatura = db.tblAsignaturas.Find(id);
            if (tblAsignatura == null)
            {
                return HttpNotFound();
            }
            return View(tblAsignatura);
        }

        // POST: tblAsignaturas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblAsignatura tblAsignatura = db.tblAsignaturas.Find(id);
            db.tblAsignaturas.Remove(tblAsignatura);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
