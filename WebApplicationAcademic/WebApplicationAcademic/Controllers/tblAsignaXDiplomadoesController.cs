﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplicationAcademic;

namespace WebApplicationAcademic.Controllers
{
    public class tblAsignaXDiplomadoesController : Controller
    {
        private dbgaEntities db = new dbgaEntities();

        // GET: tblAsignaXDiplomadoes
        public ActionResult Index()
        {
            var tblAsignaXDiplomadoes = db.tblAsignaXDiplomadoes.Include(t => t.tblAsignatura).Include(t => t.tblDiplomado);
            return View(tblAsignaXDiplomadoes.ToList());
        }

        // GET: tblAsignaXDiplomadoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblAsignaXDiplomado tblAsignaXDiplomado = db.tblAsignaXDiplomadoes.Find(id);
            if (tblAsignaXDiplomado == null)
            {
                return HttpNotFound();
            }
            return View(tblAsignaXDiplomado);
        }

        // GET: tblAsignaXDiplomadoes/Create
        public ActionResult Create()
        {
            ViewBag.idAsignatura = new SelectList(db.tblAsignaturas, "idAsignatura", "nombreAsignatura");
            ViewBag.idDiplomado = new SelectList(db.tblDiplomadoes, "idDiplomado", "nombreDiplomado");
            return View();
        }

        // POST: tblAsignaXDiplomadoes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idAsigDiplo,idAsignatura,idDiplomado,caracter,idProfesor,creditos")] tblAsignaXDiplomado tblAsignaXDiplomado)
        {
            if (ModelState.IsValid)
            {
                db.tblAsignaXDiplomadoes.Add(tblAsignaXDiplomado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idAsignatura = new SelectList(db.tblAsignaturas, "idAsignatura", "nombreAsignatura", tblAsignaXDiplomado.idAsignatura);
            ViewBag.idDiplomado = new SelectList(db.tblDiplomadoes, "idDiplomado", "nombreDiplomado", tblAsignaXDiplomado.idDiplomado);
            return View(tblAsignaXDiplomado);
        }

        // GET: tblAsignaXDiplomadoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblAsignaXDiplomado tblAsignaXDiplomado = db.tblAsignaXDiplomadoes.Find(id);
            if (tblAsignaXDiplomado == null)
            {
                return HttpNotFound();
            }
            ViewBag.idAsignatura = new SelectList(db.tblAsignaturas, "idAsignatura", "nombreAsignatura", tblAsignaXDiplomado.idAsignatura);
            ViewBag.idDiplomado = new SelectList(db.tblDiplomadoes, "idDiplomado", "nombreDiplomado", tblAsignaXDiplomado.idDiplomado);
            return View(tblAsignaXDiplomado);
        }

        // POST: tblAsignaXDiplomadoes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idAsigDiplo,idAsignatura,idDiplomado,caracter,idProfesor,creditos")] tblAsignaXDiplomado tblAsignaXDiplomado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblAsignaXDiplomado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idAsignatura = new SelectList(db.tblAsignaturas, "idAsignatura", "nombreAsignatura", tblAsignaXDiplomado.idAsignatura);
            ViewBag.idDiplomado = new SelectList(db.tblDiplomadoes, "idDiplomado", "nombreDiplomado", tblAsignaXDiplomado.idDiplomado);
            return View(tblAsignaXDiplomado);
        }

        // GET: tblAsignaXDiplomadoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblAsignaXDiplomado tblAsignaXDiplomado = db.tblAsignaXDiplomadoes.Find(id);
            if (tblAsignaXDiplomado == null)
            {
                return HttpNotFound();
            }
            return View(tblAsignaXDiplomado);
        }

        // POST: tblAsignaXDiplomadoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblAsignaXDiplomado tblAsignaXDiplomado = db.tblAsignaXDiplomadoes.Find(id);
            db.tblAsignaXDiplomadoes.Remove(tblAsignaXDiplomado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
