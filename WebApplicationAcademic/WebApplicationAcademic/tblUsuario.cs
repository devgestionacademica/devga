//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplicationAcademic
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblUsuario
    {
        public tblUsuario()
        {
            this.tblEstXDiploXAsignas = new HashSet<tblEstXDiploXAsigna>();
            this.tblMatriculas = new HashSet<tblMatricula>();
            this.tblProfesorXAsignaturas = new HashSet<tblProfesorXAsignatura>();
        }
    
        public int idUsuario { get; set; }
        public string contrasena { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string correo { get; set; }
        public Nullable<int> telefono { get; set; }
        public string direccion { get; set; }
        public int idRol { get; set; }
    
        public virtual ICollection<tblEstXDiploXAsigna> tblEstXDiploXAsignas { get; set; }
        public virtual ICollection<tblMatricula> tblMatriculas { get; set; }
        public virtual ICollection<tblProfesorXAsignatura> tblProfesorXAsignaturas { get; set; }
        public virtual tblRol tblRol { get; set; }
    }
}
